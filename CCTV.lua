CCTV = {}

local CC_IDS= {}	
CC_IDS.KD = {3,6,237,1369,1384,1443,1494,1525,1688,1755,1804,3082,3482,3581,3601,3959,4997,8018,8115,8186,8346,8412,8423,8456,8481,9028,9108,9321,9427,9422,2978,8110}
CC_IDS.SNARE = {2,431,1358,1387,1509,1542,1670,1692,1727,1825,1831,1878,1914,1927,2815,3012,3248,3262,3280,3289,3302,3311,3319,3438,3465,3468,3500,3963,3981,4213,8012,8080,8154,8255,8324,8377,8396,8484,8499,8527,9004,9057,9092,9251,9255,9318,9348,9387,9394,9479,9549,10533,10544,10555,10566,10715,14430,14900}
CC_IDS.STAGGER = {1613,3027,3028,3029,3030,3031,3032,3033,3034,3035,3168,3167,3630,3707,1524,8541,8571,8349,8038,	8237,3707}
CC_IDS.SILENCE = {1607,1683,1722,1839,3218,3616,3762,3958,8100,8174,8256,8565,8607,9030,9095,9253,9304,9388,9489,9543,9565,2983}
CC_IDS.DISARM = {1536,1837,3628,8086,8405,8495,9098,9482,9543}
CC_IDS.ROOT = {608,1370,1418,1519,1681,8024,8069,8168,8336,8449,8480,9018,9224,9334,9477,9544,8579}

local WindowName = {[1]="Stagger",[2]="Root",[3]="Snare"}
local CURRENT_KD_STUFF,CURRENT_ROOT_STUFF,CURRENT_SNARE_STUFF,CURRENT_STAGGER_STUFF,CURRENT_SILENCE_STUFF,CURRENT_DISARM_STUFF,CURRENT_THROW_STUFF = nil,nil,nil,nil,nil,nil,nil
--3082, eyeshot KD

function CCTV.Initialize()	

	CCTV.DefaultSettings = {TEXT_ALIGN = "left",SHOW_ICON = true,BORDER=4,ICON_ALIGN="left",SCALE = 1.0,LINGER=0.5,FADEOUT=0.3,ALPHA=1.0,USE_GENERIC_ICONS = false,KD=true,ROOT=true,SNARE=true,STAGGER=true,SILENCE=true,DISARM=true,THROW=true} 

	if not CCTV.SETTINGS then 
		CCTV.Reset()
	end	
	
	CCTV.ICON_ALIGN = {["left"]={BG1={top_x=-62,top_y=2,bottom_x=0,bottom_y=-2},BG2={top_x=-62,top_y=-20,bottom_x=3,bottom_y=20},ICON={Anchor_1="topleft",Anchor_2="topright",x=0,y=0}},
	["right"]={BG1={top_x=-6,top_y=2,bottom_x=56,bottom_y=-2},BG2={top_x=-8,top_y=-20,bottom_x=56,bottom_y=20},ICON={Anchor_1="topright",Anchor_2="topleft",x=-6,y=0}}}
	
	CCTV.ALIGN_ID = {["left"]=1,["right"]=2,["center"]=3,[1]="left",[2]="right",[3]="center"}
	CCTV.CC_TYPES = {[1]="KD",[2]="SNARE",[3]="SILENCE",[4]="STAGGER",[5]="DISARM",[6]="ROOT",[7]="THROW"}	
	CCTV.COOLDOWN_COLOR = { r = 20, g = 20, b = 20, a = 0.6 }
	CCTV.THROW = {CurrentX=0,CurrentY=0,PrevX=0,PrevY=0,StartX=0,StartY=0,Distance=0,Timer=0}
	CCTV.BORDER = {"tronned","Dark","Yellow","Gold","Silver","Tan","Decorative","Plain","Effect"} 	

	CCTV.CCs = {}	
	for i=1,6 do
		for key,value in pairs(CC_IDS[CCTV.CC_TYPES[i]]) do
			CCTV.CCs[value] = CCTV.CC_TYPES[i]
		end
	end

	CCTV.WORD_STAGGERED = wstring.sub(GetStringFromTable("ComponentEffects",13104),0,-2)
	CCTV.WORD_KNOCKDOWN = wstring.sub(GetStringFromTable("ComponentEffects",1526),0,-2)
	CCTV.WORD_SILENCED = wstring.sub(GetStringFromTable("ComponentEffects",16201),0,-2)
	CCTV.WORD_DISARMED = wstring.sub(GetStringFromTable("ComponentEffects",1489),0,-2)
	CCTV.WORD_ROOTED = wstring.sub(GetStringFromTable("ComponentEffects",5221),0,-2)
	CCTV.WORD_SNARED = wstring.sub(GetStringFromTable("ComponentEffects",909),0,-2)
	CCTV.WORD_THROW = L"Airborne"	
	CCTV.WORD_SECONDS = L" "..GetString(StringTables.Default.LABEL_SECONDS)

	RegisterEventHandler(SystemData.Events.PLAYER_IS_UNABLE_TO_MOVE,"CCTV.Movement")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_SILENCED,"CCTV.Cast")	
	RegisterEventHandler(SystemData.Events.PLAYER_IS_DISARMED,"CCTV.Attack")
	RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "CCTV.BuffUpdate")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "CCTV.BuffUpdate")	
	RegisterEventHandler(SystemData.Events.PLAYER_IS_BEING_THROWN,"CCTV.Throw")
	RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "CCTV.OnMove")	
	
		
	if LibSlash then LibSlash.RegisterSlashCmd("CCTV", function(input) CCTV.Slash(input) end) end
--Announce that the addon is loaded
	TextLogAddEntry("Chat", 0, L"<icon00057> CCTV v1.3 Loaded." )

--Make the main window/bar

	CreateWindow("CCTVStaggerWindow", false)
	CreateWindow("CCTVRootWindow", false)
	CreateWindow("CCTVSnareWindow", false)	
	CreateWindow("CCTVSettings", true)	

	DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", "icon004671", 64, 64) 
	DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", "icon000629", 64, 64) 
	DynamicImageSetTexture("CCTVSnareWindowICONBorderIcon", "icon002410", 64, 64) 	

	for i=1,3 do
			 
		StatusBarSetMaximumValue("CCTV"..WindowName[i].."WindowBar", 6  )
		StatusBarSetForegroundTint("CCTV"..WindowName[i].."WindowBar", DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b )
		StatusBarSetBackgroundTint("CCTV"..WindowName[i].."WindowBar", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
		StatusBarSetCurrentValue("CCTV"..WindowName[i].."WindowBar", 6  )
		StatusBarSetMaximumValue("CCTV"..WindowName[i].."WindowBar2", 6  )
		StatusBarSetForegroundTint("CCTV"..WindowName[i].."WindowBar2", DefaultColor.GREEN.r, DefaultColor.GREEN.g, DefaultColor.GREEN.b )
		StatusBarSetBackgroundTint("CCTV"..WindowName[i].."WindowBar2", DefaultColor.BLACK.r, DefaultColor.BLACK.g, DefaultColor.BLACK.b )
		StatusBarSetCurrentValue("CCTV"..WindowName[i].."WindowBar2", 6  )
		DefaultColor.SetWindowTint("CCTV"..WindowName[i].."WindowICONBorderAbilityCooldown", CCTV.COOLDOWN_COLOR )
		CooldownDisplaySetCooldown("CCTV"..WindowName[i].."WindowICONBorderAbilityCooldown", 100, 100 )
		WindowSetShowing("CCTV"..WindowName[i].."WindowICONBorderAbilityCooldown", true )
		LabelSetTextColor("CCTV"..WindowName[i].."Window"..WindowName[i].."Text",DefaultColor.GOLD.r,DefaultColor.GOLD.g,DefaultColor.GOLD.b)	
		LabelSetText("CCTV"..WindowName[i].."Window"..WindowName[i].."Time",L"10"..CCTV.WORD_SECONDS)
		LabelSetText("CCTV"..WindowName[i].."Window"..WindowName[i].."TimeBG",L"10"..CCTV.WORD_SECONDS)	
		LabelSetText("CCTV"..WindowName[i].."Window"..WindowName[i].."Name",L"Effect Name")
		LabelSetText("CCTV"..WindowName[i].."Window"..WindowName[i].."NameBG",L"Effect Name")		
		LabelSetText("CCTV"..WindowName[i].."WindowICONTypeText",L"")

	end		
	
	LabelSetText("CCTVSettingsScaleText",L"Scale")
	LabelSetText("CCTVSettingsAlphaText",GetString(StringTables.Default.LABEL_OPACITY))	
	LabelSetText("CCTVSettingsLingerText",L"Linger")		
	LabelSetText("CCTVSettingsFadeoutText",L"FadeOut")		
		
	ButtonSetText("CCTVSettingsCloseButton",L"Close")
	LabelSetText("CCTVSettingsCheckBox0Text",L"Show Icons")	
	LabelSetText("CCTVSettingsIconText",L"Generic CC Icons")

	LabelSetText("CCTVSettingsCheckBox_KD_TEXT",CCTV.WORD_KNOCKDOWN)	
	LabelSetText("CCTVSettingsCheckBox_STAGGER_TEXT",CCTV.WORD_STAGGERED)
	LabelSetText("CCTVSettingsCheckBox_ROOT_TEXT",CCTV.WORD_ROOTED)
	LabelSetText("CCTVSettingsCheckBox_SNARE_TEXT",CCTV.WORD_SNARED)
	LabelSetText("CCTVSettingsCheckBox_DISARM_TEXT",CCTV.WORD_DISARMED)
	LabelSetText("CCTVSettingsCheckBox_SILENCE_TEXT",CCTV.WORD_SILENCED)
	LabelSetText("CCTVSettingsCheckBox_THROW_TEXT",CCTV.WORD_THROW)


	LabelSetText("CCTVStaggerWindowStaggerText",towstring(CCTV.WORD_KNOCKDOWN))
	LabelSetText("CCTVStaggerWindowStaggerTextBG",towstring(CCTV.WORD_KNOCKDOWN))	
	
	LabelSetText("CCTVRootWindowRootText",towstring(CCTV.WORD_ROOTED))
	LabelSetText("CCTVRootWindowRootTextBG",towstring(CCTV.WORD_ROOTED))	
	
	LabelSetText("CCTVSnareWindowSnareText",towstring(CCTV.WORD_SNARED))
	LabelSetText("CCTVSnareWindowSnareTextBG",towstring(CCTV.WORD_SNARED))		

	ComboBoxClearMenuItems("CCTVSettingsCombobox")
	ComboBoxAddMenuItem("CCTVSettingsCombobox", L"Left")
	ComboBoxAddMenuItem("CCTVSettingsCombobox", L"Right")
	ComboBoxAddMenuItem("CCTVSettingsCombobox", L"Center")  
	ComboBoxSetSelectedMenuItem("CCTVSettingsCombobox", CCTV.ALIGN_ID[CCTV.SETTINGS.TEXT_ALIGN])
	
	ComboBoxClearMenuItems("CCTVSettingsCombobox2")
	ComboBoxAddMenuItem("CCTVSettingsCombobox2", L"Left")
	ComboBoxAddMenuItem("CCTVSettingsCombobox2", L"Right")
	ComboBoxSetSelectedMenuItem("CCTVSettingsCombobox2", CCTV.ALIGN_ID[CCTV.SETTINGS.ICON_ALIGN])
	
	ComboBoxClearMenuItems("CCTVSettingsCombobox3")
	for k,v in pairs(CCTV.BORDER) do
	ComboBoxAddMenuItem("CCTVSettingsCombobox3", towstring(v))
	end
	ComboBoxSetSelectedMenuItem("CCTVSettingsCombobox3", CCTV.SETTINGS.BORDER)
	
	CCTV.Layout = false
	CCTV.CanMove = true
	CCTV.CanCast = true
	CCTV.CanAttack = true
	CCTV.IsThrown = false
	
	SnareFade = false
	
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar1",CCTV.SETTINGS.SCALE/2)
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar2",CCTV.SETTINGS.ALPHA)
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar3",CCTV.SETTINGS.LINGER/5)	
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar4",CCTV.SETTINGS.FADEOUT/5)	

	CCTV.UpdateSettings()	
	
end

function CCTV.Shutdown()
	UnregisterEventHandler(SystemData.Events.PLAYER_IS_UNABLE_TO_MOVE,"CCTV.Movement")
	UnregisterEventHandler(SystemData.Events.PLAYER_IS_SILENCED,"CCTV.Cast")	
	UnregisterEventHandler(SystemData.Events.PLAYER_IS_DISARMED,"CCTV.Attack")
	UnregisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "CCTV.BuffUpdate")
	UnregisterEventHandler(SystemData.Events.PLAYER_IS_BEING_THROWN,"CCTV.Throw")
	UnregisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "CCTV.OnMove")	
	UnregisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "CCTV.BuffUpdate")
end

function CCTV.Throw()
	if CCTV.SETTINGS.THROW == true then
		if CCTV.IsThrown == false and GameData.Player.isBeingThrown == true then
		CCTV.THROW = {CurrentX=CCTV.THROW.CurrentX,CurrentY=CCTV.THROW.CurrentY,PrevX=0,PrevY=0,StartX=CCTV.THROW.CurrentX,StartY=CCTV.THROW.CurrentY,Distance=0,Timer=0}
		CCTV.IsThrown = true
		else
			CCTV.IsThrown = false
			GameData.Player.isBeingThrown = false
		end
	end	
end

function CCTV.OnMove(x, y)
	CCTV.THROW.CurrentX = x
	CCTV.THROW.CurrentY = y
end	


function CCTV.Movement()
	CCTV.CanMove = not CCTV.CanMove
end

function CCTV.Cast()
	CCTV.CanCast = not CCTV.CanCast
end

function CCTV.Attack()
	CCTV.CanAttack = not CCTV.CanAttack
end


function CCTV.BuffUpdate(updateType)
	
	local TestBuff = GetBuffs(GameData.BuffTargetType.SELF)
	if TestBuff ~= nil then
	
-- ====== Checking if CC's has already been applied ======

	local HasRootID = false
	local HasSnareID = false
	local HasStaggerID = false
	local HasSilenceID = false	
	local HasDisarmID = false	
	local HasKDID = false

	
	for k, v in pairs( TestBuff ) do

		if CURRENT_ROOT_STUFF ~= nil then
			if CURRENT_ROOT_STUFF.Id == v.abilityId then HasRootID = true end
		end
		if CURRENT_SNARE_STUFF ~=nil then
			if CURRENT_SNARE_STUFF.Id == v.abilityId then HasSnareID = true end
		end
		if CURRENT_STAGGER_STUFF ~=nil then
			if CURRENT_STAGGER_STUFF.Id == v.abilityId then HasStaggerID = true end
		end
		if CURRENT_KD_STUFF ~=nil then
			if CURRENT_KD_STUFF.Id == v.abilityId then HasKDID = true end
		end
		if CURRENT_SILENCE_STUFF ~=nil then
			if CURRENT_SILENCE_STUFF.Id == v.abilityId then HasSilenceID = true end
		end
		if CURRENT_DISARM_STUFF ~=nil then
			if CURRENT_DISARM_STUFF.Id == v.abilityId then HasDisarmID = true end
		end
	end	
	
	if HasRootID == false and CURRENT_ROOT_STUFF ~= nil then CURRENT_ROOT_STUFF = nil end
	if HasSnareID == false and CURRENT_SNARE_STUFF ~= nil then CURRENT_SNARE_STUFF = nil end
	if HasStaggerID == false and CURRENT_STAGGER_STUFF ~= nil then CURRENT_STAGGER_STUFF = nil end	
	if HasKDID == false and CURRENT_KD_STUFF ~= nil then CURRENT_KD_STUFF = nil end	
	if HasSilenceID == false and CURRENT_SILENCE_STUFF ~= nil then CURRENT_SILENCE_STUFF = nil end	
	if HasDisarmID == false and CURRENT_DISARM_STUFF ~= nil then CURRENT_DISARM_STUFF = nil end		
	
	
-- ====== Applying CC's ======	
		for k, v in pairs( TestBuff ) do
			if CCTV.CCs[v.abilityId] ~= nil then
			--Roots
				if CCTV.CCs[v.abilityId] == "ROOT" and (CCTV.CanMove == false or (v.abilityId==8579)) and CCTV.SETTINGS.ROOT == true then
					if CURRENT_ROOT_STUFF == nil then
						--d("adding Root stuff")					
						CURRENT_ROOT_STUFF ={}
						CURRENT_ROOT_STUFF.Id = v.abilityId
						CURRENT_ROOT_STUFF.Duration = v.duration
						CURRENT_ROOT_STUFF.MaxTimer = v.duration						
						CURRENT_ROOT_STUFF.Name = v.name						
						CURRENT_ROOT_STUFF.Text = v.effectText
						CURRENT_ROOT_STUFF.Icon = v.iconNum	
						CURRENT_ROOT_STUFF.Type = towstring(DataUtils.GetAbilityTypeText(v))	
						local _, _, _, Red,Green,Blue = DataUtils.GetAbilityTypeTextureAndColor( v )							
						CURRENT_ROOT_STUFF.Color = {r=Red,g=Green,b=Blue}						
					end
			--Snares
				elseif CCTV.CCs[v.abilityId] == "SNARE" and (v.isBuff == false or v.abilityId == 1692) and CCTV.SETTINGS.SNARE == true then
					if CURRENT_SNARE_STUFF == nil then
						--d("adding Snare stuff")	
						local _, _, _, Red,Green,Blue = DataUtils.GetAbilityTypeTextureAndColor( v )							
						CURRENT_SNARE_STUFF ={}
						CURRENT_SNARE_STUFF.Id = v.abilityId
						CURRENT_SNARE_STUFF.Duration = v.duration
						CURRENT_SNARE_STUFF.MaxTimer = v.duration				
						CURRENT_SNARE_STUFF.Name = v.name						
						CURRENT_SNARE_STUFF.Text = v.effectText	
						CURRENT_SNARE_STUFF.Icon = v.iconNum	
						CURRENT_SNARE_STUFF.Type = towstring(DataUtils.GetAbilityTypeText(v))							
						CURRENT_SNARE_STUFF.Color = {r=Red,g=Green,b=Blue}
					end
			--KnockDown
				elseif CCTV.CCs[v.abilityId] == "KD" and CCTV.CanMove == false and CCTV.SETTINGS.KD == true then
					if CURRENT_KD_STUFF == nil then
						--d("adding KnockDown stuff")
						CURRENT_KD_STUFF ={}
						CURRENT_KD_STUFF.Id = v.abilityId
						CURRENT_KD_STUFF.Duration = v.duration
						CURRENT_KD_STUFF.MaxTimer = v.duration						
						CURRENT_KD_STUFF.Name = v.name						
						CURRENT_KD_STUFF.Text = v.effectText	
						CURRENT_KD_STUFF.Icon = v.iconNum		
						CURRENT_KD_STUFF.Type = towstring(DataUtils.GetAbilityTypeText(v))
						local _, _, _, Red,Green,Blue = DataUtils.GetAbilityTypeTextureAndColor( v )							
						CURRENT_KD_STUFF.Color = {r=Red,g=Green,b=Blue}									
					end
			--Silence
				elseif CCTV.CCs[v.abilityId] == "SILENCE" and CCTV.CanCast == false and CCTV.SETTINGS.SILENCE == true then
					if CURRENT_SILENCE_STUFF == nil then
						--d("adding Silence stuff")
						CURRENT_SILENCE_STUFF ={}
						CURRENT_SILENCE_STUFF.Id = v.abilityId
						CURRENT_SILENCE_STUFF.Duration = v.duration
						CURRENT_SILENCE_STUFF.MaxTimer = v.duration						
						CURRENT_SILENCE_STUFF.Name = v.name						
						CURRENT_SILENCE_STUFF.Text = v.effectText	
						CURRENT_SILENCE_STUFF.Icon = v.iconNum		
						CURRENT_SILENCE_STUFF.Type = towstring(DataUtils.GetAbilityTypeText(v))	
						local _, _, _, Red,Green,Blue = DataUtils.GetAbilityTypeTextureAndColor( v )							
						CURRENT_SILENCE_STUFF.Color = {r=Red,g=Green,b=Blue}	
					end
			
			--Disarm
				elseif CCTV.CCs[v.abilityId] == "DISARM" and CCTV.CanAttack == false and CCTV.SETTINGS.DISARM == true then
					if CURRENT_DISARM_STUFF == nil then
						--d("adding Disarm stuff")
						CURRENT_DISARM_STUFF ={}
						CURRENT_DISARM_STUFF.Id = v.abilityId
						CURRENT_DISARM_STUFF.Duration = v.duration
						CURRENT_DISARM_STUFF.MaxTimer = v.duration							
						CURRENT_DISARM_STUFF.Name = v.name						
						CURRENT_DISARM_STUFF.Text = v.effectText	
						CURRENT_DISARM_STUFF.Icon = v.iconNum		
						CURRENT_DISARM_STUFF.Type = towstring(DataUtils.GetAbilityTypeText(v))
						local _, _, _, Red,Green,Blue = DataUtils.GetAbilityTypeTextureAndColor( v )							
						CURRENT_DISARM_STUFF.Color = {r=Red,g=Green,b=Blue}							
					end
						
			--Stagger
				elseif CCTV.CCs[v.abilityId] == "STAGGER" and CCTV.CanMove == false and CCTV.SETTINGS.STAGGER == true then
					if CURRENT_STAGGER_STUFF == nil then
						--d("adding Stagger stuff")					
						CURRENT_STAGGER_STUFF ={}
						CURRENT_STAGGER_STUFF.Id = v.abilityId
						CURRENT_STAGGER_STUFF.Duration = v.duration
						CURRENT_STAGGER_STUFF.MaxTimer = v.duration							
						CURRENT_STAGGER_STUFF.Name = v.name						
						CURRENT_STAGGER_STUFF.Text = v.effectText	
						CURRENT_STAGGER_STUFF.Icon = v.iconNum		
						CURRENT_STAGGER_STUFF.Type = towstring(DataUtils.GetAbilityTypeText(v))	
						local _, _, _, Red,Green,Blue = DataUtils.GetAbilityTypeTextureAndColor( v )							
						CURRENT_STAGGER_STUFF.Color = {r=Red,g=Green,b=Blue}						
					end	
				end			
			end
		end
	end
end


function CCTV.Update(timeElapsed)

	local ShowStagger,ShowRoot,ShowSnare,ShowLayout = false,false,false,false
	LabelSetTextColor("CCTVSnareWindowSnareTime",225, 125, 125)
	LabelSetTextColor("CCTVStaggerWindowStaggerTime",225, 125, 125)
	LabelSetTextColor("CCTVRootWindowRootTime",225, 125, 125)	
	
--Knockbacks, Pulls
	if CCTV.SETTINGS.THROW == true then
		if CCTV.IsThrown == true then
			if (CCTV.THROW.CurrentX ~= CCTV.THROW.PrevX) or (CCTV.THROW.CurrentY ~= CCTV.THROW.PrevY) then
				CCTV.THROW.Timer = CCTV.THROW.Timer + timeElapsed		
				CCTV.THROW.PrevX = CCTV.THROW.CurrentX
				CCTV.THROW.PrevY = CCTV.THROW.CurrentY
				
				local DistX = 	CCTV.THROW.StartX - CCTV.THROW.CurrentX
				local DistY = 	CCTV.THROW.StartY - CCTV.THROW.CurrentY		
				
				CCTV.THROW.Distance =  (math.sqrt(DistX*DistX+DistY*DistY))*0.086
			
			else
				GameData.Player.isBeingThrown = false
				CCTV.IsThrown = false
			end
		end
	end
--Snares
	if CURRENT_SNARE_STUFF ~= nil and CURRENT_SNARE_STUFF.Duration >= 0.01 then
	CURRENT_SNARE_STUFF.Duration = CURRENT_SNARE_STUFF.Duration - timeElapsed
			WindowStopAlphaAnimation("CCTVSnareWindow")	
			local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(CURRENT_SNARE_STUFF.Icon)
			local RedSpeed = CURRENT_SNARE_STUFF.Text:match(L".+(%d%d+)[%%].")
			if RedSpeed == "" or RedSpeed == nil then
				RedSpeed = 40
			end
			
			LabelSetTextColor("CCTVSnareWindowICONTypeText",255, 255, 255)
		--	LabelSetText("CCTVSnareWindowICONTypeText",CURRENT_SNARE_STUFF.Type)
			WindowSetTintColor("CCTVSnareWindowICONBorder",CURRENT_SNARE_STUFF.Color.r,CURRENT_SNARE_STUFF.Color.g,CURRENT_SNARE_STUFF.Color.b)
			WindowSetTintColor("CCTVSnareWindowICONBorderIcon", 255, 255, 255)
			if WindowGetShowing("CCTVSnareWindow") == false then WindowSetShowing("CCTVSnareWindow",true) end
			WindowSetShowing("CCTVSnareWindowSnareText",true)
			ShowSnare=true
			if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVSnareWindowICONBorderIcon", "icon002410", 64, 64) else DynamicImageSetTexture("CCTVSnareWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
			LabelSetText("CCTVSnareWindowSnareText",towstring(CCTV.WORD_SNARED)..L" "..towstring(RedSpeed)..L"% ")
			LabelSetText("CCTVSnareWindowSnareTextBG",towstring(CCTV.WORD_SNARED)..L" "..towstring(RedSpeed)..L"% ")
			LabelSetTextColor("CCTVSnareWindowSnareTime",255, 255, 255)			
			LabelSetText("CCTVSnareWindowSnareName",towstring(CURRENT_SNARE_STUFF.Name))
			LabelSetText("CCTVSnareWindowSnareNameBG",towstring(CURRENT_SNARE_STUFF.Name))
			LabelSetText("CCTVSnareWindowSnareTime",wstring.format(L"%.01f",(CURRENT_SNARE_STUFF.Duration))..CCTV.WORD_SECONDS)
			LabelSetText("CCTVSnareWindowSnareTimeBG",wstring.format(L"%.01f",(CURRENT_SNARE_STUFF.Duration))..CCTV.WORD_SECONDS)
			WindowSetTintColor( "CCTVSnareWindowICONBorderAbilityCooldown",20,20,20 )
			CooldownDisplaySetCooldown( "CCTVSnareWindowICONBorderAbilityCooldown", CURRENT_SNARE_STUFF.Duration, CURRENT_SNARE_STUFF.MaxTimer )
			StatusBarSetMaximumValue("CCTVSnareWindowBar", CURRENT_SNARE_STUFF.MaxTimer)
			StatusBarSetCurrentValue("CCTVSnareWindowBar", CURRENT_SNARE_STUFF.Duration)
			
			StatusBarSetMaximumValue("CCTVSnareWindowBar2", CURRENT_SNARE_STUFF.MaxTimer)
			StatusBarSetCurrentValue("CCTVSnareWindowBar2", CURRENT_SNARE_STUFF.Duration)
						
			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVSnareWindowBackgroundBorder"..CCTV.BORDER[9],CURRENT_SNARE_STUFF.Color.r,CURRENT_SNARE_STUFF.Color.g,CURRENT_SNARE_STUFF.Color.b)	
			end
			--WindowSetAlpha("CCTVSnareWindow",1)	
	end
--Silences
	if CURRENT_SILENCE_STUFF ~= nil and CURRENT_SILENCE_STUFF.Duration >= 0.01 then			
		local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(CURRENT_SILENCE_STUFF.Icon)	
		CURRENT_SILENCE_STUFF.Duration = CURRENT_SILENCE_STUFF.Duration - timeElapsed
		LabelSetTextColor("CCTVStaggerWindowICONTypeText",255, 255, 255)
	--	LabelSetText("CCTVStaggerWindowICONTypeText",CURRENT_SILENCE_STUFF.Type)
		WindowSetTintColor("CCTVStaggerWindowICONBorder",CURRENT_SILENCE_STUFF.Color.r,CURRENT_SILENCE_STUFF.Color.g,CURRENT_SILENCE_STUFF.Color.b)
		WindowSetTintColor("CCTVStaggerWindowICONBorderIcon", 255, 255, 255)
		if WindowGetShowing("CCTVStaggerWindow") == false then WindowSetShowing("CCTVStaggerWindow",true) end 
		ShowStagger=true
		if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", "icon008037", 64, 64)  else DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
		LabelSetText("CCTVStaggerWindowStaggerText",towstring(CCTV.WORD_SILENCED))
		LabelSetText("CCTVStaggerWindowStaggerTextBG",towstring(CCTV.WORD_SILENCED))		
		LabelSetText("CCTVStaggerWindowStaggerName",towstring(CURRENT_SILENCE_STUFF.Name))
		LabelSetText("CCTVStaggerWindowStaggerNameBG",towstring(CURRENT_SILENCE_STUFF.Name))		
		LabelSetTextColor("CCTVStaggerWindowStaggerTime",255, 255, 255)		
		LabelSetText("CCTVStaggerWindowStaggerTime",wstring.format(L"%.01f",(CURRENT_SILENCE_STUFF.Duration))..CCTV.WORD_SECONDS)
		LabelSetText("CCTVStaggerWindowStaggerTimeBG",wstring.format(L"%.01f",(CURRENT_SILENCE_STUFF.Duration))..CCTV.WORD_SECONDS)		
		
		WindowSetTintColor( "CCTVStaggerWindowICONBorderAbilityCooldown", 20,20,20 )
		CooldownDisplaySetCooldown( "CCTVStaggerWindowICONBorderAbilityCooldown", CURRENT_SILENCE_STUFF.Duration, CURRENT_SILENCE_STUFF.MaxTimer )
		StatusBarSetMaximumValue("CCTVStaggerWindowBar",CURRENT_SILENCE_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar",CURRENT_SILENCE_STUFF.Duration)
		
		StatusBarSetMaximumValue("CCTVStaggerWindowBar2",CURRENT_SILENCE_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar2",CURRENT_SILENCE_STUFF.Duration)
		
			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVStaggerWindowBackgroundBorder"..CCTV.BORDER[9],CURRENT_SILENCE_STUFF.Color.r,CURRENT_SILENCE_STUFF.Color.g,CURRENT_SILENCE_STUFF.Color.b)	
			end
		
	end	
	
--KnockDowns
	if CURRENT_KD_STUFF ~= nil and CURRENT_KD_STUFF.Duration >= 0.01 then
		
		local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(CURRENT_KD_STUFF.Icon)	
		CURRENT_KD_STUFF.Duration = CURRENT_KD_STUFF.Duration - timeElapsed
		LabelSetTextColor("CCTVStaggerWindowICONTypeText", 255, 255, 255)
	--	LabelSetText("CCTVStaggerWindowICONTypeText",CURRENT_KD_STUFF.Type)
		WindowSetTintColor("CCTVStaggerWindowICONBorder",CURRENT_KD_STUFF.Color.r,CURRENT_KD_STUFF.Color.g,CURRENT_KD_STUFF.Color.b)
		WindowSetTintColor("CCTVStaggerWindowICONBorderIcon", 255, 255, 255)
		if WindowGetShowing("CCTVStaggerWindow") == false then WindowSetShowing("CCTVStaggerWindow",true) end
		ShowStagger = true
		if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", "icon002643", 64, 64) else DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
		LabelSetText("CCTVStaggerWindowStaggerText",towstring(CCTV.WORD_KNOCKDOWN))
		LabelSetText("CCTVStaggerWindowStaggerTextBG",towstring(CCTV.WORD_KNOCKDOWN))		
		LabelSetText("CCTVStaggerWindowStaggerName",towstring(CURRENT_KD_STUFF.Name))
		LabelSetText("CCTVStaggerWindowStaggerNameBG",towstring(CURRENT_KD_STUFF.Name))
		LabelSetTextColor("CCTVStaggerWindowStaggerTime",255, 255, 255)		
		LabelSetText("CCTVStaggerWindowStaggerTime",wstring.format(L"%.01f",(CURRENT_KD_STUFF.Duration))..CCTV.WORD_SECONDS)
		LabelSetText("CCTVStaggerWindowStaggerTimeBG",wstring.format(L"%.01f",(CURRENT_KD_STUFF.Duration))..CCTV.WORD_SECONDS)
		WindowSetTintColor( "CCTVStaggerWindowICONBorderAbilityCooldown", 20,20,20 )
		CooldownDisplaySetCooldown( "CCTVStaggerWindowICONBorderAbilityCooldown", CURRENT_KD_STUFF.Duration, CURRENT_KD_STUFF.MaxTimer)
		StatusBarSetMaximumValue("CCTVStaggerWindowBar", CURRENT_KD_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar", CURRENT_KD_STUFF.Duration)	
		
		StatusBarSetMaximumValue("CCTVStaggerWindowBar2", CURRENT_KD_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar2", CURRENT_KD_STUFF.Duration)

			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVStaggerWindowBackgroundBorder"..CCTV.BORDER[9],CURRENT_KD_STUFF.Color.r,CURRENT_KD_STUFF.Color.g,CURRENT_KD_STUFF.Color.b)	
			end		
	end
	
--Staggers	
	if CURRENT_STAGGER_STUFF ~= nil and CURRENT_STAGGER_STUFF.Duration >= 0.01 then
		local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(CURRENT_STAGGER_STUFF.Icon)	
		CURRENT_STAGGER_STUFF.Duration = CURRENT_STAGGER_STUFF.Duration - timeElapsed
		--d(wstring.format( L"%0.1f", CURRENT_SNARE_STUFF.Duration ))
		LabelSetTextColor("CCTVStaggerWindowICONTypeText", 255, 255, 255)
	--	LabelSetText("CCTVStaggerWindowICONTypeText",CURRENT_STAGGER_STUFF.Type)
		WindowSetTintColor("CCTVStaggerWindowICONBorder",CURRENT_STAGGER_STUFF.Color.r,CURRENT_STAGGER_STUFF.Color.g,CURRENT_STAGGER_STUFF.Color.b)
		WindowSetTintColor("CCTVStaggerWindowICONBorderIcon", 255, 255, 255)
		if WindowGetShowing("CCTVStaggerWindow") == false then WindowSetShowing("CCTVStaggerWindow",true) end 
			ShowStagger = true
		if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", "icon004671", 64, 64)  else DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
		LabelSetText("CCTVStaggerWindowStaggerText",towstring(CCTV.WORD_STAGGERED))
		LabelSetText("CCTVStaggerWindowStaggerTextBG",towstring(CCTV.WORD_STAGGERED))	
		LabelSetTextColor("CCTVStaggerWindowStaggerTime",255, 255, 255)
		LabelSetText("CCTVStaggerWindowStaggerName",towstring(CURRENT_STAGGER_STUFF.Name))
		LabelSetText("CCTVStaggerWindowStaggerNameBG",towstring(CURRENT_STAGGER_STUFF.Name))
		LabelSetText("CCTVStaggerWindowStaggerTime",wstring.format(L"%.01f",(CURRENT_STAGGER_STUFF.Duration))..CCTV.WORD_SECONDS)
		LabelSetText("CCTVStaggerWindowStaggerTimeBG",wstring.format(L"%.01f",(CURRENT_STAGGER_STUFF.Duration))..CCTV.WORD_SECONDS)
		WindowSetTintColor( "CCTVStaggerWindowICONBorderAbilityCooldown", 20,20,20 )
		CooldownDisplaySetCooldown( "CCTVStaggerWindowICONBorderAbilityCooldown",CURRENT_STAGGER_STUFF.Duration,  CURRENT_STAGGER_STUFF.MaxTimer )
		StatusBarSetMaximumValue("CCTVStaggerWindowBar", CURRENT_STAGGER_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar", CURRENT_STAGGER_STUFF.Duration)
		
		StatusBarSetMaximumValue("CCTVStaggerWindowBar2", CURRENT_STAGGER_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar2", CURRENT_STAGGER_STUFF.Duration)		

			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVStaggerWindowBackgroundBorder"..CCTV.BORDER[9],CURRENT_STAGGER_STUFF.Color.r,CURRENT_STAGGER_STUFF.Color.g,CURRENT_STAGGER_STUFF.Color.b)	
			end		
	end
	
--Disarms
	if CURRENT_DISARM_STUFF ~= nil and CURRENT_DISARM_STUFF.Duration >= 0.01 then
		local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(CURRENT_DISARM_STUFF.Icon)	
		CURRENT_DISARM_STUFF.Duration = CURRENT_DISARM_STUFF.Duration - timeElapsed
		LabelSetTextColor("CCTVStaggerWindowICONTypeText",255, 255, 255)
	--	LabelSetText("CCTVStaggerWindowICONTypeText",CURRENT_DISARM_STUFF.Type)
		WindowSetTintColor("CCTVStaggerWindowICONBorder",CURRENT_DISARM_STUFF.Color.r,CURRENT_DISARM_STUFF.Color.g,CURRENT_DISARM_STUFF.Color.b)
		WindowSetTintColor("CCTVStaggerWindowICONBorderIcon", 255, 255, 255)
		if WindowGetShowing("CCTVStaggerWindow") == false then WindowSetShowing("CCTVStaggerWindow",true) end
		ShowStagger=true
		if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", "icon002644", 64, 64)  else DynamicImageSetTexture("CCTVStaggerWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
		LabelSetText("CCTVStaggerWindowStaggerText",towstring(CCTV.WORD_DISARMED))
		LabelSetText("CCTVStaggerWindowStaggerTextBG",towstring(CCTV.WORD_DISARMED))
		LabelSetText("CCTVStaggerWindowStaggerName",towstring(CURRENT_DISARM_STUFF.Name))
		LabelSetText("CCTVStaggerWindowStaggerNameBG",towstring(CURRENT_DISARM_STUFF.Name))
		LabelSetTextColor("CCTVStaggerWindowStaggerTime",255, 255, 255)		
		LabelSetText("CCTVStaggerWindowStaggerTime",wstring.format(L"%.01f",(CURRENT_DISARM_STUFF.Duration))..CCTV.WORD_SECONDS)
		LabelSetText("CCTVStaggerWindowStaggerTimeBG",wstring.format(L"%.01f",(CURRENT_DISARM_STUFF.Duration))..CCTV.WORD_SECONDS)
		WindowSetTintColor( "CCTVStaggerWindowICONBorderAbilityCooldown", 20,20,20 )
		CooldownDisplaySetCooldown( "CCTVStaggerWindowICONBorderAbilityCooldown", CURRENT_DISARM_STUFF.Duration,  CURRENT_DISARM_STUFF.MaxTimer )
		StatusBarSetMaximumValue("CCTVStaggerWindowBar", CURRENT_DISARM_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar", CURRENT_DISARM_STUFF.Duration)
		
		StatusBarSetMaximumValue("CCTVStaggerWindowBar2", CURRENT_DISARM_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVStaggerWindowBar2", CURRENT_DISARM_STUFF.Duration)		

			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVStaggerWindowBackgroundBorder"..CCTV.BORDER[9],CURRENT_DISARM_STUFF.Color.r,CURRENT_DISARM_STUFF.Color.g,CURRENT_DISARM_STUFF.Color.b)	
			end		
	end

--Roots	
	if CURRENT_ROOT_STUFF ~= nil and CURRENT_ROOT_STUFF.Duration >= 0.01 then
	CURRENT_ROOT_STUFF.Duration = CURRENT_ROOT_STUFF.Duration - timeElapsed
			
		local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(CURRENT_ROOT_STUFF.Icon)			

		LabelSetTextColor("CCTVRootWindowICONTypeText",255, 255, 255)
	--	LabelSetText("CCTVRootWindowICONTypeText",CURRENT_ROOT_STUFF.Type)
		WindowSetTintColor("CCTVRootWindowICONBorder",CURRENT_ROOT_STUFF.Color.r,CURRENT_ROOT_STUFF.Color.g,CURRENT_ROOT_STUFF.Color.b)

		WindowSetTintColor("CCTVRootWindowICONBorderIcon", 255, 255, 255)
		if WindowGetShowing("CCTVRootWindow") == false then WindowSetShowing("CCTVRootWindow",true) end
			ShowRoot=true
		if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", "icon000629", 64, 64) else DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
		LabelSetText("CCTVRootWindowRootText",towstring(CCTV.WORD_ROOTED))
		LabelSetText("CCTVRootWindowRootTextBG",towstring(CCTV.WORD_ROOTED))		
		LabelSetText("CCTVRootWindowRootName",towstring(CURRENT_ROOT_STUFF.Name))
		LabelSetText("CCTVRootWindowRootNameBG",towstring(CURRENT_ROOT_STUFF.Name))
		LabelSetTextColor("CCTVRootWindowRootTime",255, 255, 255)		
		LabelSetText("CCTVRootWindowRootTime",wstring.format(L"%.01f",(CURRENT_ROOT_STUFF.Duration))..CCTV.WORD_SECONDS)
		LabelSetText("CCTVRootWindowRootTimeBG",wstring.format(L"%.01f",(CURRENT_ROOT_STUFF.Duration))..CCTV.WORD_SECONDS)
		WindowSetTintColor( "CCTVRootWindowICONBorderAbilityCooldown",20,20,20 )
		CooldownDisplaySetCooldown( "CCTVRootWindowICONBorderAbilityCooldown", CURRENT_ROOT_STUFF.Duration, CURRENT_ROOT_STUFF.MaxTimer)
		StatusBarSetMaximumValue("CCTVRootWindowBar",  CURRENT_ROOT_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVRootWindowBar", CURRENT_ROOT_STUFF.Duration)
		
		StatusBarSetMaximumValue("CCTVRootWindowBar2",  CURRENT_ROOT_STUFF.MaxTimer)
		StatusBarSetCurrentValue("CCTVRootWindowBar2", CURRENT_ROOT_STUFF.Duration)
		
			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVRootWindowBackgroundBorder"..CCTV.BORDER[9],CURRENT_ROOT_STUFF.Color.r,CURRENT_ROOT_STUFF.Color.g,CURRENT_ROOT_STUFF.Color.b)	
			end		
	end

--Knockbacks,Pulls	
	if CCTV.SETTINGS.THROW == true and CURRENT_ROOT_STUFF == nil and CCTV.IsThrown == true then
			
		local CCTV_IconTEXTURE,CCTV_IconX,CCTV_IconY = GetIconData(13392)			

		LabelSetTextColor("CCTVRootWindowICONTypeText",255, 255, 255)
	--	LabelSetText("CCTVRootWindowICONTypeText",CURRENT_ROOT_STUFF.Type)
		WindowSetTintColor("CCTVRootWindowICONBorder",255, 255, 255)

		WindowSetTintColor("CCTVRootWindowICONBorderIcon", 255, 255, 255)
		if WindowGetShowing("CCTVRootWindow") == false then WindowSetShowing("CCTVRootWindow",true) end
		ShowRoot=true
		if CCTV.SETTINGS.USE_GENERIC_ICONS == true then DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", "icon013392", 64, 64) else DynamicImageSetTexture("CCTVRootWindowICONBorderIcon", CCTV_IconTEXTURE, CCTV_IconX, CCTV_IconY) end
		LabelSetText("CCTVRootWindowRootText",CCTV.WORD_THROW)
		LabelSetText("CCTVRootWindowRootTextBG",CCTV.WORD_THROW)		
		LabelSetText("CCTVRootWindowRootName",L"Distance: "..wstring.format(L"%.01f",(CCTV.THROW.Distance))..L"ft")
		LabelSetText("CCTVRootWindowRootNameBG",L"Distance: "..wstring.format(L"%.01f",(CCTV.THROW.Distance))..L"ft")
		LabelSetTextColor("CCTVRootWindowRootTime",255, 255, 255)			
		LabelSetText("CCTVRootWindowRootTime",wstring.format(L"%.01f",(CCTV.THROW.Timer))..CCTV.WORD_SECONDS)
		LabelSetText("CCTVRootWindowRootTimeBG",wstring.format(L"%.01f",(CCTV.THROW.Timer))..CCTV.WORD_SECONDS)
		WindowSetTintColor( "CCTVRootWindowICONBorderAbilityCooldown",20,20,20 )
		CooldownDisplaySetCooldown( "CCTVRootWindowICONBorderAbilityCooldown",0, 0)
		StatusBarSetMaximumValue("CCTVRootWindowBar",  10)
		StatusBarSetCurrentValue("CCTVRootWindowBar", CCTV.THROW.Timer)
		
		StatusBarSetMaximumValue("CCTVRootWindowBar2",  10)
		StatusBarSetCurrentValue("CCTVRootWindowBar2", CCTV.THROW.Timer)
		
			if CCTV.SETTINGS.BORDER == 9 then
				WindowSetTintColor("CCTVRootWindowBackgroundBorder"..CCTV.BORDER[9],255,255,255)	
			end			
	end	
	
	
	if WindowGetShowing("CCTVStaggerWindow") == false then
		WindowClearAnchors( "CCTVRootWindow" )
		WindowAddAnchor( "CCTVRootWindow" , "top", "CCTVStaggerWindow", "top",0,0)	
	else
		WindowClearAnchors( "CCTVRootWindow" )
		WindowAddAnchor( "CCTVRootWindow" , "bottom", "CCTVStaggerWindow", "top",0,5)
	end

	if WindowGetShowing("CCTVRootWindow") == false then
		WindowClearAnchors( "CCTVSnareWindow" )
		WindowAddAnchor( "CCTVSnareWindow" , "top", "CCTVRootWindow", "top",0,0)		
	else
		WindowClearAnchors( "CCTVSnareWindow" )
		WindowAddAnchor( "CCTVSnareWindow" , "bottom", "CCTVRootWindow", "top",0,5)
	end


	if CCTV.Layout == true then
		WindowSetHandleInput("CCTVStaggerWindow",true)
		WindowSetShowing("CCTVSettings",true)
		ShowLayout = true
		CCTV.UpdateSettings()
		
		for i=1,3 do
		WindowSetShowing("CCTV"..WindowName[i].."Window",true)	
		WindowSetAlpha("CCTV"..WindowName[i].."Window",1)		
		WindowSetAlpha("CCTV"..WindowName[i].."Window"..WindowName[i].."Text",1)		
		end

	else
		WindowSetHandleInput("CCTVStaggerWindow",false)
		WindowSetHandleInput("CCTVRootWindow",false)
		WindowSetHandleInput("CCTVSnareWindow",false)		
		WindowSetShowing("CCTVSettings",false)
		ShowLayout = false
	end

--	WindowSetShowing("CCTVStaggerWindow",(ShowStagger or ShowLayout))
--	WindowSetShowing("CCTVRootWindow",(ShowRoot or ShowLayout))

	if (ShowStagger or ShowLayout) == true then WindowStartAlphaAnimation("CCTVStaggerWindow", Window.AnimationType.EASE_OUT,1, 0, (CCTV.SETTINGS.FADEOUT)+0.1, true, CCTV.SETTINGS.LINGER, 0) end
	if (ShowRoot or ShowLayout) == true then WindowStartAlphaAnimation("CCTVRootWindow", Window.AnimationType.EASE_OUT,1, 0, (CCTV.SETTINGS.FADEOUT)+0.1, true, CCTV.SETTINGS.LINGER, 0) end
	if (ShowSnare or ShowLayout) == true then WindowStartAlphaAnimation("CCTVSnareWindow", Window.AnimationType.EASE_OUT,1, 0, (CCTV.SETTINGS.FADEOUT)+0.1, true, CCTV.SETTINGS.LINGER, 0) end


	if WindowGetAlpha("CCTVSnareWindow") <= 0 then
		WindowStopAlphaAnimation("CCTVSnareWindow")
		WindowSetShowing("CCTVSnareWindow",false) 
		WindowSetAlpha("CCTVSnareWindow",1) 
	end	
	
	if WindowGetAlpha("CCTVRootWindow") <= 0 then
		WindowStopAlphaAnimation("CCTVRootWindow")
		WindowSetShowing("CCTVRootWindow",false) 
		WindowSetAlpha("CCTVRootWindow",1) 
	end	
	
	if WindowGetAlpha("CCTVStaggerWindow") <= 0 then
		WindowStopAlphaAnimation("CCTVStaggerWindow")
		WindowSetShowing("CCTVStaggerWindow",false) 
		WindowSetAlpha("CCTVStaggerWindow",1) 
	end	

end

function CCTV.UpdateSettings()

	CCTV.SETTINGS.FADEOUT = SliderBarGetCurrentPosition("CCTVSettingsSliderBar4")*5
	CCTV.SETTINGS.LINGER = SliderBarGetCurrentPosition("CCTVSettingsSliderBar3")*5
	CCTV.SETTINGS.SCALE = SliderBarGetCurrentPosition("CCTVSettingsSliderBar1")*2
	CCTV.SETTINGS.ALPHA = SliderBarGetCurrentPosition("CCTVSettingsSliderBar2")

	for i=1,#CCTV.BORDER do
		WindowSetShowing("CCTVStaggerWindowBackgroundBorder"..CCTV.BORDER[i],false)	
		WindowSetShowing("CCTVSnareWindowBackgroundBorder"..CCTV.BORDER[i],false)	
		WindowSetShowing("CCTVRootWindowBackgroundBorder"..CCTV.BORDER[i],false)			
	end
	WindowSetShowing("CCTVStaggerWindowBackgroundBorder"..CCTV.BORDER[CCTV.SETTINGS.BORDER],true)
	WindowSetShowing("CCTVSnareWindowBackgroundBorder"..CCTV.BORDER[CCTV.SETTINGS.BORDER],true)
	WindowSetShowing("CCTVRootWindowBackgroundBorder"..CCTV.BORDER[CCTV.SETTINGS.BORDER],true)	

--	LabelSetText("CCTVSettingsScaleText",L"Scale: "..wstring.format(L"%.01f",(CCTV.SETTINGS.SCALE)))	
	LabelSetText("CCTVSettingsScaleText",L"Scale: "..towstring(math.ceil(CCTV.SETTINGS.SCALE*100))..L"%")	
--	LabelSetText("CCTVSettingsAlphaText",GetString(StringTables.Default.LABEL_OPACITY)..L": "..wstring.format(L"%.01f",(CCTV.SETTINGS.ALPHA)))
	LabelSetText("CCTVSettingsAlphaText",GetString(StringTables.Default.LABEL_OPACITY)..L": "..towstring(math.ceil(CCTV.SETTINGS.ALPHA*100))..L"%")
	LabelSetText("CCTVSettingsLingerText",L"Linger: "..wstring.format(L"%.01f",(CCTV.SETTINGS.LINGER))..L"s")
	LabelSetText("CCTVSettingsFadeoutText",L"FadeOut: "..wstring.format(L"%.01f",(CCTV.SETTINGS.FADEOUT))..L"s")

	SliderBarSetCurrentPosition("CCTVSettingsSliderBar2",CCTV.SETTINGS.ALPHA)
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar1",CCTV.SETTINGS.SCALE/2)
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar3",CCTV.SETTINGS.LINGER/5)
	SliderBarSetCurrentPosition("CCTVSettingsSliderBar4",CCTV.SETTINGS.FADEOUT/5)

	ButtonSetPressedFlag( "CCTVSettingsCheckBox_KD",CCTV.SETTINGS.KD)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox_SNARE",CCTV.SETTINGS.SNARE)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox_SILENCE",CCTV.SETTINGS.SILENCE)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox_STAGGER",CCTV.SETTINGS.STAGGER)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox_DISARM",CCTV.SETTINGS.DISARM)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox_ROOT",CCTV.SETTINGS.ROOT)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox_THROW",CCTV.SETTINGS.THROW)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox0",CCTV.SETTINGS.SHOW_ICON)
	ButtonSetPressedFlag( "CCTVSettingsCheckBox1",CCTV.SETTINGS.USE_GENERIC_ICONS)

	for i=1,3 do
		
		WindowSetDimensions("CCTV"..WindowName[i].."Window",160,64)	
		LabelSetTextAlign("CCTV"..WindowName[i].."Window"..WindowName[i].."Text", CCTV.SETTINGS.TEXT_ALIGN)
		LabelSetTextAlign("CCTV"..WindowName[i].."Window"..WindowName[i].."TextBG", CCTV.SETTINGS.TEXT_ALIGN)
		LabelSetTextAlign("CCTV"..WindowName[i].."Window"..WindowName[i].."Name", CCTV.SETTINGS.TEXT_ALIGN)
		LabelSetTextAlign("CCTV"..WindowName[i].."Window"..WindowName[i].."NameBG", CCTV.SETTINGS.TEXT_ALIGN)
		LabelSetTextAlign("CCTV"..WindowName[i].."Window"..WindowName[i].."Time", CCTV.SETTINGS.TEXT_ALIGN)
		LabelSetTextAlign("CCTV"..WindowName[i].."Window"..WindowName[i].."TimeBG", CCTV.SETTINGS.TEXT_ALIGN)
		WindowSetTintColor( "CCTV"..WindowName[i].."WindowICONBorderAbilityCooldown", 20,20,20 )
		WindowSetScale("CCTV"..WindowName[i].."Window",tonumber(CCTV.SETTINGS.SCALE)+0.1)
		WindowSetAlpha("CCTV"..WindowName[i].."WindowBackground",tonumber(CCTV.SETTINGS.ALPHA))	


		if 	CCTV.SETTINGS.SHOW_ICON == false then
			WindowSetShowing("CCTV"..WindowName[i].."WindowICON",false)
			WindowClearAnchors( "CCTV"..WindowName[i].."WindowBackground2" )
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground2" , "topleft", "CCTV"..WindowName[i].."Window", "topleft",-8,-20)
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground2" , "bottomright", "CCTV"..WindowName[i].."Window", "bottomright",3,20)		
			WindowClearAnchors( "CCTV"..WindowName[i].."WindowBackground" )
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground" , "topleft", "CCTV"..WindowName[i].."Window", "topleft",-6,2)
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground" , "bottomright", "CCTV"..WindowName[i].."Window", "bottomright",0,-2)
		else	
			local Icon_Align = CCTV.ICON_ALIGN[CCTV.SETTINGS.ICON_ALIGN]
			WindowSetShowing("CCTV"..WindowName[i].."WindowICON",true)
			WindowClearAnchors( "CCTV"..WindowName[i].."WindowICON" )
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowICON" , Icon_Align.ICON.Anchor_1, "CCTV"..WindowName[i].."Window", Icon_Align.ICON.Anchor_2,Icon_Align.ICON.x,Icon_Align.ICON.y)			
			WindowClearAnchors( "CCTV"..WindowName[i].."WindowBackground2" )
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground2" , "topleft", "CCTV"..WindowName[i].."Window", "topleft",Icon_Align.BG2.top_x,Icon_Align.BG2.top_y)
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground2" , "bottomright", "CCTV"..WindowName[i].."Window", "bottomright",Icon_Align.BG2.bottom_x,Icon_Align.BG2.bottom_y)
			WindowClearAnchors( "CCTV"..WindowName[i].."WindowBackground" )
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground" , "topleft", "CCTV"..WindowName[i].."Window", "topleft",Icon_Align.BG1.top_x,Icon_Align.BG1.top_y)
			WindowAddAnchor( "CCTV"..WindowName[i].."WindowBackground" , "bottomright", "CCTV"..WindowName[i].."Window", "bottomright",Icon_Align.BG1.bottom_x,Icon_Align.BG1.bottom_y)				
		end

		if CCTV.SETTINGS.TEXT_ALIGN == "center" then
				WindowSetShowing("CCTV"..WindowName[i].."WindowBar",true)
				WindowClearAnchors("CCTV"..WindowName[i].."WindowBar")
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar" , "topleft", "CCTV"..WindowName[i].."WindowTimerBar", "topleft",74,0)
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar" , "bottomright", "CCTV"..WindowName[i].."WindowTimerBar", "bottomright",0,0)				
				WindowSetShowing("CCTV"..WindowName[i].."WindowBar2",true)
				WindowClearAnchors("CCTV"..WindowName[i].."WindowBar2")
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar2", "topleft", "CCTV"..WindowName[i].."WindowTimerBar", "topleft",0,0)
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar2", "bottomright", "CCTV"..WindowName[i].."WindowTimerBar", "bottomright",-74,0)
		elseif CCTV.SETTINGS.TEXT_ALIGN == "left" then
				WindowSetShowing("CCTV"..WindowName[i].."WindowBar",true)
				WindowClearAnchors( "CCTV"..WindowName[i].."WindowBar" )
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar" , "topleft", "CCTV"..WindowName[i].."WindowTimerBar", "topleft",0,0)
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar" , "bottomright", "CCTV"..WindowName[i].."WindowTimerBar", "bottomright",0,0)
				WindowSetShowing("CCTV"..WindowName[i].."WindowBar2",false)
		elseif CCTV.SETTINGS.TEXT_ALIGN == "right" then		
				WindowSetShowing("CCTV"..WindowName[i].."WindowBar2",true)
				WindowClearAnchors("CCTV"..WindowName[i].."WindowBar2")
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar2", "topleft", "CCTV"..WindowName[i].."WindowTimerBar", "topleft",0,0)
				WindowAddAnchor("CCTV"..WindowName[i].."WindowBar2", "bottomright", "CCTV"..WindowName[i].."WindowTimerBar", "bottomright",0,0)
				WindowSetShowing("CCTV"..WindowName[i].."WindowBar",false)
		end
	end

return
end


function CCTV.Slash(input)
	if (input == "") then
		CCTV.Layout = not CCTV.Layout
	elseif (input == "reset") then
		CCTV.Reset()	
	end
end

function CCTV.Reset()
	CCTV.SETTINGS = CCTV.DefaultSettings
end

function CCTV.Close()
	CCTV.Layout = false	
end

function CCTV.MenuSelect(idx)
	CCTV.SETTINGS.TEXT_ALIGN = CCTV.ALIGN_ID[idx]
end

function CCTV.MenuSelect2(idx)
	CCTV.SETTINGS.ICON_ALIGN = CCTV.ALIGN_ID[idx]
end

function CCTV.MenuSelect3(idx)
	 CCTV.SETTINGS.BORDER = idx
end

function CCTV.ICONCHANGE()
	CCTV.SETTINGS.USE_GENERIC_ICONS = not CCTV.SETTINGS.USE_GENERIC_ICONS
end

function CCTV.ICONENABLE()
	CCTV.SETTINGS.SHOW_ICON = not CCTV.SETTINGS.SHOW_ICON
end

function CCTV.TOGGLE_TYPE()
	local BoxNumber	= WindowGetId (SystemData.ActiveWindow.name)
	CCTV.SETTINGS[CCTV.CC_TYPES[BoxNumber]] = not CCTV.SETTINGS[CCTV.CC_TYPES[BoxNumber]]
end

function CCTV.Toggle()
	if WindowGetShowing("CCTVRootWindow") == true then
		WindowStartAlphaAnimation( "CCTVRootWindowBackground2", Window.AnimationType.LOOP, 1.0, 0.0, 0.4, true, 0.0, 0 ) --start the pulse
	else
		WindowStopAlphaAnimation("CCTVRootWindowBackground2")
	end  
end

function CCTV.Toggle2()
	if WindowGetShowing("CCTVStaggerWindow") == true then
		WindowStartAlphaAnimation( "CCTVStaggerWindowBackground2", Window.AnimationType.LOOP, 1.0, 0.0, 0.4, true, 0.0, 0 ) --start the pulse
	else
		WindowStopAlphaAnimation("CCTVStaggerWindowBackground2")
	end  
end

function CCTV.Toggle3()
	if WindowGetShowing("CCTVSnareWindow") == true then
		WindowStartAlphaAnimation( "CCTVSnareWindowBackground2", Window.AnimationType.LOOP, 1.0, 0.0, 0.4, true, 0.0, 0 ) --start the pulse
	else
		WindowStopAlphaAnimation("CCTVSnareWindowBackground2")
	end  
end

