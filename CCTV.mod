<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="CCTV" version="1.4" date="14/8/2019">
	<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" /> 
     <Author name="Sullemunk" />
        <Description text="CCTV (CrowdControll TimerViewer),Alerts and Shows timers when you are effected by CC's. Made By Sullemunk for RoR" />
       <Dependencies>           
      		<Dependency name="LibSlash" optional="false" forceEnable="true" />
			
        </Dependencies>
        <Files>
            <File name="CCTV.lua" />
            <File name="CCTV.xml" />
        </Files>
				<SavedVariables>		

	<SavedVariable name="CCTV.SETTINGS" />		
	
				</SavedVariables>
        <OnInitialize>
            <CallFunction name="CCTV.Initialize" />
        </OnInitialize>
        <OnUpdate>
			<CallFunction name="CCTV.Update" />		
    	</OnUpdate>
		
        <OnShutdown>
			<CallFunction name="CCTV.Shutdown" />
		</OnShutdown>
    </UiMod>
</ModuleFile>